<?php


namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints\DependsOnType;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema()
 */
class Contact
{
    /**
     * @var integer|null
     * @Assert\Type(type={"integer","null"})
     * @Assert\Positive()
     * @OA\Property(type="integer",example=1)
     */
    protected $id;
    /**
     * @var integer
     * @Assert\Type("integer")
     * @Assert\Positive()
     * @OA\Property(type="integer",example=1)
     */
    protected $contact_type;

    /**
     * @var string
     * @Assert\NotBlank()
     * @DependsOnType()
     * @OA\Property(type="string")
     */
    protected $contact;

    public function __construct(int $contact_type, string $contact, int $id = null)
    {
        $this->id = $id;
        $this->contact_type = $contact_type;
        $this->contact = $contact;
    }

    /**
     * @return int
     */
    public function getContactType(): int
    {
        return $this->contact_type;
    }

    /**
     * @return string
     */
    public function getContact(): string
    {
        return $this->contact;
    }

    /**
     * @param string $contact
     */
    public function setContact(string $contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @param int $contact_type
     */
    public function setContactType(int $contact_type): void
    {
        $this->contact_type = $contact_type;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function hasId(): bool
    {
        return isset($this->id);
    }
}