<?php


namespace App\Dto;


use App\Dto\Schema\ContactWithVerification;
use App\Dto\Schema\RoleWithRights;
use App\Entity\RoleRight;
use App\Entity\User;
use App\Entity\UserContact;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\SerializedName;
use App\Validator\Constraints\CheckFullName;


/**
 * @OA\Schema()
 */
class FullUserResponseDto
{
    /**
     * @var int
     * @Assert\Type("integer")
     * @Assert\Positive()
     * @SerializedName("id")
     * @OA\Property(type="integer",example=1)
     */
    public $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @CheckFullName()
     * @OA\Property(type="string")
     * @SerializedName("full_name")
     */
    protected $full_name;

    /**
     * @var ContactWithVerification[]
     * @SerializedName("contacts")
     * @OA\Property(ref="#/components/schemas/ContactWithVerification")
     */
    public $contacts;

    /**
     * @var array
     * @Assert\Type("object")
     * @SerializedName("role")
     * @OA\Property(type="object",ref="#/components/schemas/RoleWithRights")
     */
    public $role;

    public function __construct(User $user)
    {
        $role = $user->getRole();
        $this->id = $user->getId();
        $this->full_name = $user->getFullName();

        $this->contacts = array_map(function (UserContact $contact) {
            return new ContactWithVerification($contact->getType(),
                $contact->getContact(),
                $contact->getId(),
                $contact->getIsVerified());
        }, $user->getUserContacts()->toArray());

        $this->role = new RoleWithRights($role->getId(), $role->getName(), $role->getAlias(),
            array_map(function (RoleRight $roleRight) {
                return $roleRight->getRight()->getAlias();
            }, $role->getRoleRight()->toArray()));
    }
}