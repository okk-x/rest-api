<?php


namespace App\Dto;

use App\Validator\Constraints\ConfirmPassword;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(required={"user_id","restore_token","password","password_confirmation"})
 */
class PasswordRecoveryRequest extends RequestDto
{
    /**
     * @var int
     * @Assert\Type("integer")
     * @Assert\Positive()
     * @OA\Property(type="integer",example=1)
     */
    protected $user_id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    protected $restore_token;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=6)
     * @OA\Property(type="string")
     */
    protected $password;
    /**
     * @var string
     * @ConfirmPassword
     * @Assert\NotBlank()
     * @Assert\Length(min=6)
     * @OA\Property(type="string")
     */
    protected $password_confirmation;


    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @return string
     */
    public function getRestoreToken(): string
    {
        return $this->restore_token;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getPasswordConfirmation(): string
    {
        return $this->password_confirmation;
    }


}