<?php


namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(required={"type","device_uuid"})
 */
class LogoutRequest extends RequestDto
{
    /**
     * @var string
     * @Assert\Choice({"current","all"})
     * @OA\Property(type="string")
     */
    protected $type;
    /**
     * @var string
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    protected $device_uuid;

    /**
     * @return string
     */
    public function getDeviceUuid(): string
    {
        return $this->device_uuid;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}