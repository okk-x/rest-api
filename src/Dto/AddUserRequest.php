<?php


namespace App\Dto;


use App\Dto\Contact;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints\CheckFullName;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @OA\Schema()
 */
class AddUserRequest extends RequestDto
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @CheckFullName()
     * @OA\Property(type="string")
     * @SerializedName("full_name")
     */
    protected $full_name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min=6)
     * @OA\Property(type="string")
     * @SerializedName("password")
     */
    protected $password;

    /**
     * @var int
     * @Assert\Type("integer")
     * @Assert\Positive()
     * @OA\Property(type="integer",example=1)
     * @SerializedName("role_id")
     */
    protected $role_id;

    /**
     * @var int
     * @Assert\Type("integer")
     * @Assert\PositiveOrZero()
     * @OA\Property(type="integer")
     * @SerializedName("token_life_time")
     */
    protected $token_life_time;

    /**
     * @var array
     * @Assert\All({
     *     @Assert\Type(type="App\DTO\Contact"),
     * })
     * @Assert\Valid
     * @OA\Property(type="array",@OA\Items(ref="#/components/schemas/Contact"))
     * @SerializedName("contacts")
     */
    protected $contacts;

    /**
     * @param Request $request
     * @param int $filled
     * @throws \Exception
     */

    public function __construct(Request $request, int $filled = self::ALL_FIELDS_FILLED)
    {
        parent::__construct($request, $filled);

        $contacts = $this->contacts = array_map(function ($contact) {
            return new Contact($contact['contact_type'], $contact['contact']);
        }, $this->contacts);

        $this->contacts = new ArrayCollection($contacts);
    }

    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return array
     */
    public function getContacts(): array
    {
        return $this->contacts->toArray();
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->full_name;
    }

    /**
     * @return mixed
     */
    public function getRoleId()
    {
        return $this->role_id;
    }

    /**
     * @return mixed
     */
    public function getTokenLifeTime()
    {
        return $this->token_life_time;
    }

}