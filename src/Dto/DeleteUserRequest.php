<?php


namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(required={"id"})
 */
class DeleteUserRequest extends RequestDto
{
    /**
     * @Assert\Type(type={"int","null"})
     * @Assert\Positive()
     * @OA\Property(type="integer",example=1)
     */
    protected $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}