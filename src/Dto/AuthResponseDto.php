<?php


namespace App\Dto;


use App\Dto\Schema\RoleWithRights;
use App\Dto\Schema\Token;
use App\Entity\RoleRight;
use App\Entity\UserToken;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\SerializedName;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema()
 */
class AuthResponseDto implements ResponseDtoInterface
{
    /**
     * @var int
     * @Assert\Type("integer")
     * @Assert\Positive()
     * @SerializedName("id")
     * @OA\Property(type="integer",example=1)
     */
    public $id;

    /**
     * @var string
     * @Assert\Type("string")
     * @Assert\NotBlank()
     * @SerializedName("full_name")
     * @OA\Property(type="string")
     */
    public $full_name;

    /**
     * @var array
     * @Assert\Type("object")
     * @SerializedName("token")
     * @OA\Property(type="object",ref="#/components/schemas/Token")
     */
    public $token;

    /**
     * @var array
     * @Assert\Type("object")
     * @SerializedName("role")
     * @OA\Property(type="object",ref="#/components/schemas/RoleWithRights")
     */
    public $role;

    public function __construct(UserToken $token)
    {
        $user = $token->getUser();
        $role = $user->getRole();
        $rights = array_map(function (RoleRight $roleRight) {
            return $roleRight->getRight()->getAlias();
        }, $role->getRoleRight()->toArray());

        $this->id = $user->getId();
        $this->full_name = $user->getFullName();

        $this->token=new Token($token->getId(),$token->getToken(),
            ($user->getTokenLifeTime() === 0) ?
            $user->getTokenLifeTime() :
            $token->getUpdateTs()->add(new \DateInterval("PT{$user->getTokenLifeTime()}S")));

        $this->role=new RoleWithRights($role->getId(),$role->getName(),$role->getAlias(),$rights);
    }
}