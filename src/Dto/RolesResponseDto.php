<?php


namespace App\Dto;

use App\Entity\Role;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @OA\Schema()
 */
class RolesResponseDto implements ResponseDtoInterface
{
    /**
     * @var array
     * @OA\Property(type="array",@OA\Items(type="object",ref="#/components/schemas/Role"))
     * @SerializedName("roles")
     */
    public $roles;

    public function __construct(ArrayCollection $roles)
    {
        $this->roles=[];
        $this->roles[]=array_map(function (Role $role) {
            return new \App\Dto\Schema\Role($role->getId(),$role->getName(),$role->getAlias());
        }, $roles->toArray());
    }
}