<?php


namespace App\Dto;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema()
 */
class GetRoleRequest extends RequestDto
{
    /**
     * @var array|null
     * @Assert\NotBlank(allowNull=true)
     * @Assert\All({
     *      @Assert\Type("integer"),
     *      @Assert\Positive()
     * })
     * @OA\Property(type="array",@OA\Items(type="integer",example={1,2,3}))
     */
    protected $ids;

    /**
     * @var string|null
     * @Assert\NotBlank(allowNull=true)
     * @OA\Property(type="string")
     */
    protected $name;

    /**
     * @var array|null
     * @Assert\NotBlank(allowNull=true)
     * @Assert\All({
     *      @Assert\Type("string"),
     *      @Assert\NotBlank()
     * })
     * @OA\Property(type="array",@OA\Items(type="string"))
     */
    protected $aliases;

    public function __construct(Request $request, int $filled = self::NOT_ALL_FIELDS_FILLED)
    {
        parent::__construct($request, $filled);
    }

    /**
     * @return array|null
     */
    public function getIds(): ?array
    {
        return $this->ids;
    }

    /**
     * @return array|null
     */
    public function getAliases(): ?array
    {
        return $this->aliases;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

}