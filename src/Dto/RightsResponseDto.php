<?php


namespace App\Dto;

use App\Dto\Schema\Right;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @OA\Schema()
 */
class RightsResponseDto implements ResponseDtoInterface
{
    /**
     * @var array
     * @Assert\Type("array")
     * @OA\Property(type="array",@OA\Items(type="object",ref="#/components/schemas/Right"))
     * @SerializedName("rights")
     */
    public $rights;

    public function __construct(ArrayCollection $collection, ?int $roleId=null)
    {
        $this->rights=[];

        foreach ($collection as $right) {
            $this->rights[] = new Right($right->getId(),$right->getName(),$right->getAlias(),$roleId);
        }
    }
}