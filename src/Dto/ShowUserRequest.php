<?php


namespace App\Dto;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema()
 */
class ShowUserRequest extends RequestDto
{
    /**
     * @Assert\Type(type={"int","null"})
     * @Assert\Positive()
     * @OA\Property(type="integer",example=1)
     */
    protected $id;

    public function __construct(Request $request, int $filled = self::NOT_ALL_FIELDS_FILLED)
    {
        parent::__construct($request, $filled);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function hasId(): bool
    {
        return isset($this->id);
    }
}