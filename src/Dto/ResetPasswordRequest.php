<?php


namespace App\Dto;

use App\Validator\Constraints\DependsOnType;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

/**
 * Class ResetPasswordRequest
 * @OA\Schema(required={"contact_type","contact"})
 */
class ResetPasswordRequest extends RequestDto
{
    /**
     * @var integer
     * @Assert\Type("integer")
     * @Assert\Positive()
     * @OA\Property(type="integer",example=1)
     */
    protected $contact_type;

    /**
     * @var string
     * @Assert\NotBlank()
     * @DependsOnType()
     * @OA\Property(type="string")
     */
    protected $contact;

    /**
     * @return int
     */
    public function getContactType(): int
    {
        return $this->contact_type;
    }

    /**
     * @return string
     */
    public function getContact(): string
    {
        return $this->contact;
    }
}