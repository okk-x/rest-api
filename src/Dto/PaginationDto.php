<?php


namespace App\Dto;

use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @OA\Schema()
 */
class PaginationDto
{
    /**
     * @var int
     * @OA\Property(type="integer",default=0)
     * @Assert\GreaterThanOrEqual(0)
     */
    private $start;
    /**
     * @var int
     * @OA\Property(type="integer",default=100)
     * @Assert\GreaterThanOrEqual(1)
     */
    private $limit;
    /**
     * @var int
     * @OA\Property(type="integer")
     * @Assert\GreaterThanOrEqual(0)
     */
    private $total;

    public function __construct(int $start, int $limit, int $total=0)
    {
        $this->start = $start;
        $this->limit = $limit;
        $this->total = $total;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getStart(): int
    {
        return $this->start;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }
}