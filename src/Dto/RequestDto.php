<?php


namespace App\Dto;

use App\Exception\RequestValidationException;
use mysql_xdevapi\Exception;
use ReflectionProperty;
use Symfony\Component\HttpFoundation\Request;

abstract class RequestDto implements RequestDtoInterface
{
    protected const NOT_ALL_FIELDS_FILLED = 0;
    protected const ALL_FIELDS_FILLED = 1;
    /**
     * @var int
     */
    private $filled;

    public function __construct(Request $request, int $filled = self::ALL_FIELDS_FILLED)
    {
        $this->filled = $filled;

        if (!$this->checkFilled($filled)) {
            throw new \Exception('Constant not defined in class ' . __CLASS__ . ' method ' . __METHOD__);
        }

        $this->setPropertiesFromRequest(json_decode($request->getContent(), 1));

    }

    protected function checkFilled(int $filled): bool
    {
        return in_array($filled, (new \ReflectionClass($this))->getConstants());
    }

    protected function setPropertiesFromRequest($request)
    {
        $properties = (new \ReflectionClass($this))->getProperties(ReflectionProperty::IS_PUBLIC
            | ReflectionProperty::IS_PROTECTED);

        if (!count($properties))
            throw new \RuntimeException('Properties in class ' . __CLASS__ . ' are not defined');

        foreach ($properties as $property) {
            if (!isset($request[$property->name])) {
                if ($this->filled === self::ALL_FIELDS_FILLED) {
                    throw new \Exception('Not all fields are filled. Field ' . $property->name);
                } else if ($this->filled === self::NOT_ALL_FIELDS_FILLED) {
                    continue;
                }
            }

            $this->{$property->name} = $request[$property->name];
        }
    }

}