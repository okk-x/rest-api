<?php


namespace App\Dto;


use Doctrine\Common\Collections\ArrayCollection;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @OA\Schema()
 */
class FullUsersResponseDto
{
    /**
     * @var array
     * @OA\Property(type="array",@OA\Items(ref="#/components/schemas/FullUserResponseDto"))
     * @SerializedName("users")
     */
    public $users;

    /**
     * @var PaginationDto
     * @OA\Property(type="object",@OA\Items(ref="#/components/schemas/PaginationDto"))
     * @SerializedName("pagination")
     */
    public $pagination;

    public function __construct(ArrayCollection $users, PaginationDto $pagination)
    {
        $this->users = [];
        foreach ($users as $user) {
            $this->users[] = new FullUserResponseDto($user);
        }
        $this->pagination=$pagination;
    }
}