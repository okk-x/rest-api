<?php


namespace App\Dto;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(required={"id","verification_token"})
 */
class VerificationRequest extends RequestDto
{

    /**
     * @var int
     * @Assert\Type("integer")
     * @Assert\Positive()
     * @OA\Property(type="integer",example=1)
     */
    protected $id;
    /**
     * @var string
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    protected $verification_token;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getVerificationToken(): string
    {
        return $this->verification_token;
    }
}