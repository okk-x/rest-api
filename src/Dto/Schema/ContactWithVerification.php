<?php


namespace App\Dto\Schema;


use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints\DependsOnType;

/**
 * @OA\Schema()
 */
class ContactWithVerification
{
    /**
     * @var integer|null
     * @Assert\Type(type={"integer","null"})
     * @Assert\Positive()
     * @OA\Property(type="integer",example=1)
     */
    public $id;
    /**
     * @var integer
     * @Assert\Type("integer")
     * @Assert\Positive()
     * @OA\Property(type="integer",example=1)
     */
    public $contact_type;

    /**
     * @var string
     * @Assert\NotBlank()
     * @DependsOnType()
     * @OA\Property(type="string")
     */
    public $contact;

    /**
     * @var bool
     * @OA\Property(type="integer",example=1)
     * @Assert\Type("boolean")
     */
    private $is_verified;

    public function __construct(int $contact_type, string $contact, int $id, bool $isVerified)
    {

        $this->is_verified = $isVerified;
        $this->contact_type = $contact_type;
        $this->contact = $contact;
        $this->id = $id;
    }
}