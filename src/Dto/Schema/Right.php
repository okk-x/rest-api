<?php


namespace App\Dto\Schema;


use Symfony\Component\Validator\Constraints as Assets;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @OA\Schema()
 */
class Right
{
    /**
     * @var int
     * @Assert\Type("int")
     * @Assert\Positive()
     * @OA\Property(type="integer",example="1")
     */
    public $id;

    /**
     * @var string
     * @Assert\Type("string")
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    public $name;

    /**
     * @var string
     * @Assert\Type("string")
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    public $alias;

    /**
     * @var int|null
     * @Assert\Type({"int","null"})
     * @Assert\Positive()
     * @OA\Property(type="integer",example="1",nullable=true)
     */
    public $role_id;

    public function __construct(int $id,string $name,string $alias,?int $role_id)
    {
        $this->id = $id;
        $this->name = $name;
        $this->alias = $alias;
        $this->role_id = $role_id;
    }
}