<?php


namespace App\Dto\Schema;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @OA\Schema()
 */
class Token
{
    /**
     * @var int
     * @Assert\Type("int"),
     * @Assert\Positive()
     * @OA\Property(type="integer",example=1)
     */
    public $id;

    /**
     * @var string
     * @Assert\Type("string"),
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    public $token;

    /**
     * @var string
     * @Assert\Date()
     * @OA\Property(type="string")
     */
    public $expiration_ts;

    public function __construct(int $id, string $token,string $expiration_ts)
    {
        $this->id = $id;
        $this->token = $token;
        $this->expiration_ts = $expiration_ts;
    }
}