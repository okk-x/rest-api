<?php


namespace App\Dto\Schema;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;


/**
 * @OA\Schema()
 */
class RoleWithRights
{
    /**
     * @var int
     * @Assert\Type("int")
     * @Assert\Positive()
     * @OA\Property(type="integer",example=1)
     */
    public $id;

    /**
     * @var string
     * @Assert\Type("string")
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    public $name;

    /**
     * @var string
     * @Assert\Type("string")
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    public $alias;

    /**
     * @var array
     * @Assert\All({
     *          @Assert\NotBlank,
     *          @Assert\Type("string")
     * })
     * @OA\Property(type="array",@OA\Items(type="string"))
     */
    public $rights;

    public function __construct(int $id,string $name,string $alias,array $rights)
    {
        $this->id = $id;
        $this->name = $name;
        $this->alias = $alias;
        $this->rights = $rights;
    }

}