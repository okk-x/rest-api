<?php


namespace App\Dto\Schema;

use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @OA\Schema()
 */
class Order
{
    /**
     * @var string
     * @Assert\Choice({"id","full_name","created_ts","last_login_ts"})
     * @OA\Property(type="string")
     */
    public $field;

    /**
     * @var string
     * @Assert\Choice({"asc","desc"})
     * @OA\Property(type="string")
     */
    public $direction;

    public function __construct(string $field,string $direction)
    {
        $this->direction = $direction;
        $this->field = $field;
    }
}