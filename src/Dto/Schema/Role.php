<?php


namespace App\Dto\Schema;

use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;


/**
 * @OA\Schema()
 */
class Role
{
    /**
     * @var int
     * @Assert\Type("int")
     * @Assert\Positive()
     * @OA\Property(type="integer",example="1")
     */
    public $id;

    /**
     * @var string
     * @Assert\Type("string")
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    public $name;

    /**
     * @var string
     * @Assert\Type("string")
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    public $alias;

    public function __construct(int $id,string $name,string $alias)
    {

        $this->alias = $alias;
        $this->id = $id;
        $this->name = $name;
    }
}