<?php


namespace App\Dto;


use App\Dto\Schema\Order;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(required={"order","pagination"})
 */
class GetUserRequest extends RequestDto
{
    private const PAGINATION_LIMIT = 100;
    private const PAGINATION_START = 0;

    /**
     * @Assert\NotBlank(allowNull=true)
     * @Assert\All({
     *      @Assert\Type("integer"),
     *      @Assert\Positive()
     * })
     * @OA\Property(type="array",@OA\Items(type="integer"))
     */
    protected $ids;

    /**
     * @Assert\NotBlank(allowNull=true)
     * @OA\Property(type="string")
     */
    protected $full_name;

    /**
     * @Assert\NotBlank(allowNull=true)
     * @Assert\All({
     *      @Assert\Type("integer"),
     *      @Assert\Positive()
     * })
     * @OA\Property(type="array",@OA\Items(type="integer"))
     */
    protected $role_ids;

    /**
     * @Assert\NotBlank(allowNull=true)
     * @OA\Property(type="string")
     */
    protected $contact;

    /**
     * @Assert\Choice(callback={"App\Entity\User","getUserStatuses"})
     * @OA\Property(type="integer")
     */
    protected $status;

    /**
     * @Assert\Date
     * @var string A "Y-m-d" formatted value
     */
    protected $created_from;

    /**
     * @Assert\Date
     * @var string A "Y-m-d" formatted value
     * @OA\Property(type="string")
     */
    protected $created_till;

    /**
     * @Assert\Date
     * @var string A "Y-m-d" formatted value
     * @OA\Property(type="string")
     */
    protected $last_login_from;

    /**
     * @Assert\Date
     * @var string A "Y-m-d" formatted value
     * @OA\Property(type="string")
     */
    protected $last_login_till;

    /**
     * @OA\Property(type="object",@OA\Items(type="#/components/schemas/Order"))
     */
    protected $order;

    /**
     * @OA\Property(type="object",@OA\Items(type="#/components/schemas/PaginationDto"))
     */
    protected $pagination;


    public function __construct(Request $request, int $filled = self::NOT_ALL_FIELDS_FILLED)
    {
        parent::__construct($request, $filled);
        if ($this->order) {
            $this->order = new Order($this->order['field'], $this->order['direction']);
        }
        if ($this->pagination) {
            $this->pagination = new PaginationDto($this->pagination['start'], $this->pagination['limit']);
        }else{
            $this->setDefaultPagination();
        }
    }

    protected function setDefaultPagination()
    {
        $this->pagination=new PaginationDto(self::PAGINATION_START,self::PAGINATION_LIMIT);
    }

    /**
     * @return mixed
     */
    public function getFullName(): ?string
    {
        return $this->full_name;
    }

    /**
     * @return mixed
     */
    public function getContact(): ?string
    {
        return $this->contact;
    }

    /**
     * @return string
     */
    public function getCreatedFrom(): ?string
    {
        return $this->created_from;
    }

    /**
     * @return string
     */
    public function getCreatedTill(): ?string
    {
        return $this->created_till;
    }


    /**
     * @return mixed
     */
    public function getIds(): ?array
    {
        return $this->ids;
    }

    /**
     * @return string
     */
    public function getLastLoginFrom(): ?string
    {
        return $this->last_login_from;
    }

    /**
     * @return string
     */
    public function getLastLoginTill(): ?string
    {
        return $this->last_login_till;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return mixed
     */
    public function getPagination()
    {
        return $this->pagination;
    }

    /**
     * @return mixed
     */
    public function getRoleIds()
    {
        return $this->role_ids;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

}