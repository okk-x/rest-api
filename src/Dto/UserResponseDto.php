<?php


namespace App\Dto;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\SerializedName;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema()
 */
class UserResponseDto implements ResponseDtoInterface
{
    /**
     * @var int
     * @Assert\Type("integer")
     * @Assert\Positive()
     * @OA\Property(type="integer",example=1)
     * @SerializedName("id")
     */
    public $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}