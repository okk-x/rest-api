<?php


namespace App\Dto;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema()
 */
class SuccessResponseDto implements ResponseDtoInterface
{
    /**
     * @var bool
     * @Assert\Type("bool")
     * @OA\Property(type="boolean")
     * @SerializedName("success")
     */
    public $success;

    public function __construct(bool $success)
    {
        $this->success = $success;
    }


}