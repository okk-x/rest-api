<?php


namespace App\Dto;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints\DependsOnType;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(required={"contact_type","contact","password","device_uuid","device_name"})
 */
class AuthorizationRequest extends RequestDto
{

    /**
     * @var int
     * @Assert\Type("integer")
     * @Assert\Positive()
     * @OA\Property(type="integer",example=1)
     */
    protected $contact_type;
    /**
     * @var string
     * @Assert\NotBlank()
     * @DependsOnType()
     * @OA\Property(type="string")
     */
    protected $contact;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min=6)
     * @OA\Property(type="string")
     */
    protected $password;

    /**
     * @var string
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    protected $device_uuid;
    /**
     * @var string
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    protected $device_name;


    /**
     * @return string
     */
    public function getContact(): string
    {
        return $this->contact;
    }

    /**
     * @return int
     */
    public function getContactType(): int
    {
        return $this->contact_type;
    }

    /**
     * @return string
     */
    public function getDeviceName(): string
    {
        return $this->device_name;
    }

    /**
     * @return string
     */
    public function getDeviceUuid(): string
    {
        return $this->device_uuid;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}