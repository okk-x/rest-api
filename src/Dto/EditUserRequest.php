<?php


namespace App\Dto;

use App\Dto\Contact;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints\CheckFullName;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema()
 */
class EditUserRequest extends RequestDto
{
    /**
     * @var int
     * @Assert\Type("int")
     * @Assert\Positive()
     * @OA\Property(type="integer",example=1)
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @CheckFullName()
     * @OA\Property(type="string")
     */
    protected $full_name;

    /**
     * @var int
     * @Assert\Type("int")
     * @Assert\Positive()
     * @OA\Property(type="integer",example=1)
     */
    protected $role_id;

    /**
     * @var int
     * @Assert\Type("int")
     * @Assert\PositiveOrZero()
     * @OA\Property(type="integer")
     */
    protected $token_life_time;

    /**
     * @var array
     * @Assert\All({
     *     @Assert\Type(type="App\DTO\Contact"),
     * })
     * @Assert\Valid
     * @OA\Property(type="array",@OA\Items(ref="#/components/schemas/Contact"))
     */
    protected $contacts;

    public function __construct(Request $request, int $filled = self::ALL_FIELDS_FILLED)
    {
        parent::__construct($request, $filled);

        $contacts = $this->contacts = array_map(function ($contact) {
            return new Contact($contact['contact_type'], $contact['contact'], isset($contact['id']) ? $contact['id'] : null);
        }, $this->contacts);

    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getRoleId(): int
    {
        return $this->role_id;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->full_name;
    }

    /**
     * @return int
     */
    public function getTokenLifeTime(): int
    {
        return $this->token_life_time;
    }

    /**
     * @return array
     */
    public function getContacts(): array
    {
        return $this->contacts;
    }
}