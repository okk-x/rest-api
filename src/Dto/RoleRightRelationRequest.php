<?php


namespace App\Dto;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(required={"role_id", "right_id"})
 */
class RoleRightRelationRequest extends RequestDto
{
    /**
     * @var int
     * @Assert\Type("int")
     * @Assert\Positive()
     * @OA\Property(type="integer",example=1)
     */
    protected $role_id;

    /**
     * @var int
     * @Assert\Type("int")
     * @Assert\Positive()
     * @OA\Property(type="integer",example=1)
     */
    protected $right_id;

    /**
     * @return int
     */
    public function getRoleId(): int
    {
        return $this->role_id;
    }

    /**
     * @return int
     */
    public function getRightId(): int
    {
        return $this->right_id;
    }
}