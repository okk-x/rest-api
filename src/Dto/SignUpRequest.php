<?php


namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints\CheckFullName;
use App\Validator\Constraints\ConfirmPassword;
use App\Validator\Constraints\DependsOnType;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema()
 */
class SignUpRequest extends RequestDto
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @CheckFullName()
     * @OA\Property(type="string")
     */
    protected $fullname;
    /**
     * @var integer
     * @Assert\Type("integer")
     * @Assert\Positive()
     * @OA\Property(type="integer",example=1)
     */
    protected $contact_type;

    /**
     * @var string
     * @Assert\NotBlank()
     * @DependsOnType()
     * @OA\Property(type="string")
     */
    protected $contact;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min=6)
     * @OA\Property(type="string")
     */
    protected $password;
    /**
     * @var string
     * @ConfirmPassword
     * @Assert\NotBlank()
     * @Assert\Length(min=6)
     * @OA\Property(type="string")
     */
    protected $password_confirmation;


    /**
     * @return string
     */
    public function getContact(): ?string
    {
        return $this->contact;
    }

    /**
     * @return int
     */
    public function getContactType(): ?int
    {
        return $this->contact_type;
    }

    /**
     * @return string
     */
    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

}