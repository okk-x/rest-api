<?php


namespace App\Service;


use App\Builder\UserBuilder;
use App\Dto\AddUserRequest;
use App\Dto\AuthorizationRequest;
use App\Dto\SignUpRequest;
use App\Entity\Role;
use App\Entity\User;
use App\Entity\UserContact;
use App\Entity\UserToken;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AuthService
{
    private const LOGOUT_ALL = 'all';
    private const LOGOUT_CURRENT = "current";

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var PasswordHasher
     */
    private $hasher;
    /**
     * @var Tokenizer
     */
    private $tokenizer;
    /**
     * @var UserBuilder
     */
    private $userBuilder;

    public function __construct(
        EntityManagerInterface $entityManager,
        PasswordHasher $hasher,
        Tokenizer $tokenizer,
        UserBuilder $userBuilder
    )
    {
        $this->entityManager = $entityManager;
        $this->hasher = $hasher;
        $this->tokenizer = $tokenizer;
        $this->userBuilder = $userBuilder;
    }

    public function register(SignUpRequest $request): User
    {
        $this->userBuilder->setFullname($request->getFullname())
            ->setPassword($this->hasher->hash($request->getPassword()))
            ->setStatus(User::USER_NOT_ACTIVE)
            ->setTokenLifeTime(0)
            ->setContact(new UserContact($request->getContactType(), $request->getContact(), $this->tokenizer->generate()));

        $this->setDefaultRole($this->userBuilder);
        $user = $this->userBuilder->build();

        $this->entityManager->persist($user);
        $this->entityManager->flush();
        return $user;
    }


    public function add(AddUserRequest $request): User
    {
        $this->userBuilder->setFullname($request->getFullname())
            ->setPassword($this->hasher->hash($request->getPassword()))
            ->setTokenLifeTime($request->getTokenLifeTime())
            ->setStatus(User::USER_ACTIVE);

        $this->setDefaultRole($this->userBuilder);

        $contacts = $request->getContacts();

        if (count($contacts)) {
            foreach ($contacts as $contact) {
                $this->userBuilder->setContact(
                    new UserContact(
                        $contact->getContactType(),
                        $contact->getContact(),
                        $this->tokenizer->generate()));
            }
        }

        $user = $this->userBuilder->build();
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    protected function setDefaultRole(UserBuilder $builder)
    {
        $role = $this->entityManager->getRepository(Role::class)->defaultRole();
        $builder->setRole($role);
    }

    public function authorization(AuthorizationRequest $request, string $ip): UserToken
    {
        $user = $this->entityManager->getRepository(User::class)
            ->findAuthUser($request->getContactType(),
                $request->getContact(),
                $this->hasher->hash($request->getPassword()));

        if (!$user) {
            throw new NotFoundHttpException("User not found");
        }

        $token = $this->createAuthToken($request->getDeviceUuid(), $request->getDeviceName(), $ip);
        $user->addUserToken($token);
        $user->setLastLoginTs(new \DateTimeImmutable());

        $this->entityManager->flush();

        return $token;
    }

    public function logout(string $deviceUuid, string $type): void
    {
        if (!in_array($type, [self::LOGOUT_ALL, self::LOGOUT_CURRENT])) {
            throw new HttpException('Type mismatch', 400);
        }

        switch ($type) {
            case self::LOGOUT_CURRENT:
                $this->logoutCurrent($deviceUuid);
                break;
            case self::LOGOUT_ALL:
                $this->logoutAll($deviceUuid);
                break;
        }
    }

    private function logoutCurrent(string $deviceUuid)
    {
        $userToken = $this->entityManager->getRepository(UserToken::class)->findOneBy(['device_uuid' => $deviceUuid]);
        $this->entityManager->remove($userToken);
        $this->entityManager->flush();
    }

    private function logoutAll(string $deviceUuid)
    {
        $user = $this->entityManager->getRepository(User::class)->findByDeviceUuid($deviceUuid);
        if (!$user) {
            throw new NotFoundHttpException("User not found");
        }

        foreach ($user->getUserTokens() as $userToken) {
            $this->entityManager->remove($userToken);
        }
        $this->entityManager->flush();
    }

    private function createAuthToken(string $deviceUuid, string $deviceName, string $ip): UserToken
    {
        return new UserToken($this->tokenizer->generate(), $ip, $deviceUuid, $deviceName);
    }


}