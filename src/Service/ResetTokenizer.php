<?php


namespace App\Service;


use App\Entity\ResetPassword;
use Ramsey\Uuid\Uuid;

class ResetTokenizer
{
    /**
     * @var \DateInterval
     */
    private $interval;

    public function __construct(\DateInterval $interval)
    {
        $this->interval = $interval;
    }

    public function generate()
    {
        return new ResetPassword(
            Uuid::uuid4()->toString(),
            (new \DateTimeImmutable())->add($this->interval)
        );
    }
}