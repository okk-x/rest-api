<?php


namespace App\Service;


use App\Dto\RoleRightRelationRequest;
use App\Entity\Right;
use App\Entity\Role;
use App\Entity\RoleRight;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RoleRightService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(RoleRightRelationRequest $request): void
    {
        $rr = new RoleRight();

        $role = $this->entityManager->getRepository(Role::class)->find($request->getRoleId());
        if (!$role) {
            throw new NotFoundHttpException('RoleWithRights not found');
        }
        $rr->setRole($role);

        $right = $this->entityManager->getRepository(Right::class)->find($request->getRightId());
        if (!$right) {
            throw new NotFoundHttpException('Right not found');
        }
        $rr->setRight($right);

        $this->entityManager->persist($rr);
        $this->entityManager->flush();
    }
}