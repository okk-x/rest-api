<?php


namespace App\Service;

use App\Dto\PasswordRecoveryRequest;
use App\Entity\User;
use App\Entity\UserContact;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class ResettingPasswordService
{
    private const SENDING_INTERVAL_MINUTES = 1;
    private const NUMBER_OF_REQUESTS = 10;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var string
     */
    private $template;
    /**
     * @var Tokenizer
     */
    private $tokenizer;
    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var PasswordHasher
     */
    private $hasher;
    /**
     * @var string
     */
    private $from;

    /**
     * ResettingPasswordService constructor.
     * @param EntityManagerInterface $entityManager
     * @param SessionInterface $session
     * @param MailerInterface $mailer
     * @param ResetTokenizer $tokenizer
     * @param PasswordHasher $hasher
     * @param string $template
     * @param string $from
     */
    public function __construct(EntityManagerInterface $entityManager,
                                SessionInterface $session,
                                MailerInterface $mailer,
                                ResetTokenizer $tokenizer,
                                PasswordHasher $hasher,
                                string $template,
                                string $from)
    {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->template = $template;
        $this->tokenizer = $tokenizer;
        $this->session = $session;
        $this->hasher = $hasher;
        $this->from = $from;
    }

    public function reset(PasswordRecoveryRequest $request)
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $request->getUserId()]);
        if (!$user) {
            throw new NotFoundHttpException("User doesn't exist");
        }


        $resetPassword = $user->getResetPassword();

        if (!$resetPassword) {
            throw new NotFoundHttpException('Reset token not found');
        }

        if ($request->getRestoreToken() !== $resetPassword->getCode()) {
            throw new HttpException(400, 'Token mismatch');
        }

        if ($resetPassword->isExpiredTo(new \DateTimeImmutable('now'))) {
            throw new HttpException(400, 'Token expired');
        }

        $user->setPassword($this->hasher->hash($request->getPassword()));
        $resetPassword->clear();

        $this->entityManager->flush();
    }


    public function requestByEmail(int $contactType, string $contact): void
    {
        if (!$this->isEmail($contactType)) {
            throw new HttpException('Contact type must be email', 400);
        }

        $userContact = $this->entityManager->getRepository(UserContact::class)->findOneBy(['type' => $contactType, 'contact' => $contact]);

        if (!$userContact) {
            throw new NotFoundHttpException("Contact doesn't exist");
        }

        $user = $userContact->getUser();
        $resetPassword = $user->getResetPassword();

        if (!$this->checkInterval()) {
            throw new HttpException(400, "Email was sent. You can repeat after 1 minute");
        }
        if (!$this->isRequestLimit()) {
            throw new HttpException(400, "You cannot make more 10 requests per day");
        }

        if (!isset($resetPassword) || ((isset($resetPassword) && $resetPassword->isExpiredTo(new \DateTimeImmutable('now'))))) {
            $this->generateResetToken($user);
        }

        $email = (new TemplatedEmail())
            ->from($this->from)
            ->to(new Address($userContact->getContact()))
            ->subject('Reset Password')
            ->htmlTemplate('reset-password.html.twig')
            ->context([
                'url' => $this->generateUrl($user->getId(),$user->getResetPassword()->getCode())
            ]);

        $this->mailer->send($email);
    }

    private function checkInterval(): bool
    {
        if ($this->session->has('count_request')) {
            $wasSent = $this->session->get('count_request')['date'];
            return ($wasSent->diff(new \DateTimeImmutable('now')))->i >= self::SENDING_INTERVAL_MINUTES;
        }

        return true;
    }


    private function isRequestLimit()
    {
        if ($this->session->has('count_request')) {
            $result = $this->session->get('count_request');
            $days = $result['date']->diff(new \DateTimeImmutable())->days;

            if ($result['count_request'] >= self::NUMBER_OF_REQUESTS &&
                $days < 1) {
                return false;
            } else if ($result['count_request'] >= self::NUMBER_OF_REQUESTS && $days >= 1) {
                $this->session->set('count_request', ['count_request' => 1, 'date' => new \DateTimeImmutable()]);
            } else if ($result['count_request'] === self::NUMBER_OF_REQUESTS) {
                $result['count_request']++;
                $this->session->set('count_request', $result);
            }
        } else {
            $this->session->set('count_request', ['count_request' => 1, 'date' => new \DateTimeImmutable()]);
        }
        return true;
    }

    private function generateUrl(int $userId, string $resetToken): string
    {
        return str_replace(['%user_id%', '%reset_token%'], [$userId, $resetToken], $this->template);
    }

    private function isEmail(int $contactType): bool
    {
        return $contactType === UserContact::TYPE_EMAIL;
    }

    public function generateResetToken(User $user): void
    {
        $resetPassword = $this->tokenizer->generate();

        $user->setResetPassword($resetPassword);
        $this->entityManager->flush();
    }
}