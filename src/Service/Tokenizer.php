<?php


namespace App\Service;


use Ramsey\Uuid\Uuid;

class Tokenizer
{
    public function generate(): string
    {
        return Uuid::uuid4()->toString();
    }
}