<?php


namespace App\Service;


class PasswordHasher
{
    public function hash(string $password): string
    {
        $hash = hash("sha256", $password);
        if ($hash === false)
            throw new \RuntimeException("Unable to generate hash");
        return $hash;
    }
}