<?php


namespace App\Service\OptionsService;


use App\Dto\GetRightRequest;
use App\Entity\Right;
use App\Entity\Role;
use App\Entity\RoleRight;
use Doctrine\ORM\QueryBuilder;

class RightOptionsService extends OptionsService
{
    public function __construct(GetRightRequest $request, QueryBuilder $builder)
    {
        parent::__construct($request, $builder);
        $this->builder->select('r')->from(Right::class, 'r');
    }

    public function ids(array $ids)
    {
        $this->builder->andWhere('r.id IN (:ids)')->setParameter(':ids', $ids);
    }

    public function name(string $name)
    {
        $this->builder->andWhere('r.name=:name')->setParameter(':name', $name);
    }

    public function aliases(array $aliases)
    {
        $this->builder->andWhere('r.alias IN (:aliases)')->setParameter(':aliases', $aliases);
    }

    public function role_id(int $id)
    {
        $this->builder->innerJoin(RoleRight::class, 'rr')
            ->andWhere('rr.role=:id')
            ->setParameter(':id', $id);
    }
}