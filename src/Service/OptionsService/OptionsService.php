<?php


namespace App\Service\OptionsService;


use App\Dto\GetUserRequest;
use App\Dto\RequestDto;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;

abstract class OptionsService
{
    /**
     * @var GetUserRequest
     */
    protected $request;
    protected $builder;

    public function __construct(RequestDto $request, QueryBuilder $builder)
    {
        $this->request = $request;
        $this->builder = $builder;
    }

    public function properties()
    {
        $properties = (new \ReflectionClass($this->request))->getProperties();
        return array_map(function ($property) {
            return $property->getName();
        }, $properties);
    }

    public function execute(): ?ArrayCollection
    {
        foreach ($this->properties() as $property) {
            $method = $this->requestMethod($property);
            if (!method_exists($this->request, $method)) {
                continue;
            }

            $arg = call_user_func([$this->request, $method]);
            if (!isset($arg)) {
                continue;
            }

            call_user_func([$this, $property], $arg);
        }
        $result = new ArrayCollection($this->builder->getQuery()->getResult());
        return $result->count() ? $result : null;
    }

    protected function requestMethod(string $property)
    {
        return 'get' . ucfirst(str_replace('_', '', ucwords($property, '_')));
    }
}