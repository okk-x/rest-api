<?php


namespace App\Service\OptionsService;


use App\Dto\GetRoleRequest;
use App\Dto\RequestDto;
use App\Entity\Role;
use Doctrine\ORM\QueryBuilder;

class RoleOptionsService extends OptionsService
{
    public function __construct(GetRoleRequest $request, QueryBuilder $builder)
    {
        parent::__construct($request, $builder);
        $this->builder->select('r')->from(Role::class, 'r');
    }

    public function ids(array $ids)
    {
        $this->builder->andWhere('r.id IN (:ids)')->setParameter(':ids', $ids);
    }

    public function name(string $name)
    {
        $this->builder->andWhere('r.name=:name')->setParameter(':name', $name);
    }

    public function aliases(array $aliases)
    {
        $this->builder->andWhere('r.alias IN (:aliases)')->setParameter(':aliases', $aliases);
    }
}