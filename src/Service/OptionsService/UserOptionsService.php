<?php


namespace App\Service\OptionsService;


use App\Dto\GetUserRequest;
use App\Dto\PaginationDto;
use App\Dto\Schema\Order;
use App\Entity\User;
use Doctrine\ORM\QueryBuilder;

class UserOptionsService extends OptionsService
{
    public function __construct(GetUserRequest $request, QueryBuilder $builder)
    {
        parent::__construct($request, $builder);
        $this->builder->select('u', 'uc')->from(User::class, 'u')->innerJoin('u.userContacts', 'uc');
    }

    protected function ids(array $ids)
    {
        $this->builder->andWhere('u.id IN (:ids)')
            ->setParameter(':ids', $ids);
    }

    protected function full_name(string $fullName)
    {
        $this->builder->andWhere('full_name=:fullName')
            ->setParameter(':fullName', $fullName);
    }

    protected function role_ids(array $roleIds)
    {
        $this->builder->innerJoin('u.role', 'r')
            ->andWhere('r.id IN (:ids)')
            ->setParameter(':ids', $roleIds);
    }

    protected function contact(string $contact)
    {
        $this->builder
            //->innerJoin('u.userContacts', 'uc')
            ->andWhere('uc.contact LIKE :contact')
            ->setParameter(':contact', '%' . $contact . '%');
    }

    protected function status(int $status)
    {
        $this->builder->andWhere('u.status=:status')
            ->setParameter(':status', $status);
    }

    protected function created_from(string $createdFrom)
    {
        $this->builder->andWhere('u.created_ts >= :createdFrom')
            ->setParameter(":createdFrom", $createdFrom);
    }

    protected function created_till(string $createdTill)
    {
        $this->builder->andWhere('u.created_ts <= :createdTill')
            ->setParameter(":createdTill", $createdTill);
    }

    protected function last_login_from(string $lastLoginFrom)
    {
        $this->builder->andWhere('u.last_login_ts >= :lastLoginFrom')
            ->setParameter(":lastLoginFrom", $lastLoginFrom);
    }

    protected function last_login_till(string $lastLoginTill)
    {
        $this->builder->andWhere('u.last_login_ts <= :lastLoginTill')
            ->setParameter(":lastLoginTill", $lastLoginTill);
    }

    protected function order(Order $order)
    {
        $this->builder->orderBy('u.' . $order->field, $order->direction);
    }

    protected function pagination(PaginationDto $pagination)
    {
        $this->builder->setFirstResult($pagination->getTotal())->setMaxResults($pagination->getLimit());
    }
}