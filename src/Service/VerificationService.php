<?php


namespace App\Service;


use App\Entity\User;
use App\Entity\UserContact;
use App\Repository\UserContactRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class VerificationService
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var string
     */
    private $template;
    /**
     * @var UserContactRepository
     */
    private $repository;
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var string
     */
    private $from;

    public function __construct(MailerInterface $mailer, EntityManagerInterface $entityManager, string $template, string $from)
    {
        $this->mailer = $mailer;
        $this->template = $template;
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(UserContact::class);
        $this->from = $from;
    }

    public function sendToEmail(User $user)
    {
        $contact = $this->repository->findContactWithEmail($user);

        if (!$contact) {
            throw new NotFoundHttpException("Contact with email doesn't exist");
        }

        $email = (new TemplatedEmail())
            ->from($this->from)
            ->to(new Address($contact->getContact()))
            ->subject('Verification account')
            ->htmlTemplate('signup.html.twig')
            ->context([
                'url' => $this->generateUrl($contact)
            ]);

        $this->mailer->send($email);
    }

    private function generateUrl(UserContact $contact): string
    {
        return str_replace(['%contact_id%', '%verification_token%'],
            [$contact->getId(), $contact->getVerificationToken()],
            $this->template);
    }

    public function process(int $contactId, string $verificationToken)
    {
        $contact = $this->repository->find($contactId);

        if (!isset($contact)) {
            throw new NotFoundHttpException('Contact doesn not exists');
        }

        $this->isVerified($contact);

        $this->verifyToken($contact->getVerificationToken(), $verificationToken);

        $this->verify($contact);

        $this->entityManager->flush();
    }

    private function isVerified(UserContact $contact)
    {
        if ($contact->getIsVerified() === UserContact::VERIFIED) {
            throw new HttpException("Contact already verified", 409);
        }
    }

    private function verifyToken($storedToken, $requestToken)
    {
        if ($storedToken !== $requestToken) {
            throw new HttpException("Token mismatch");
        }
    }

    private function verify(UserContact $contact)
    {
        $contact->setIsVerified(UserContact::VERIFIED);
        $contact->getUser()->setStatus(User::USER_ACTIVE);
    }


}