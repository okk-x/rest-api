<?php


namespace App\Service;

use App\Dto\Contact;
use App\Dto\EditUserRequest;
use App\Entity\Role;
use App\Entity\User;
use App\Entity\UserContact;
use Doctrine\ORM\EntityManagerInterface;
use mysql_xdevapi\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;

class UserService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var Security
     */
    private $security;

    private $users;
    /**
     * @var Tokenizer
     */
    private $tokenizer;

    public function __construct(EntityManagerInterface $em, Security $security, Tokenizer $tokenizer)
    {
        $this->em = $em;
        $this->users = $this->em->getRepository(User::class);
        $this->security = $security;
        $this->tokenizer = $tokenizer;
    }


    public function getAuthUser(): ?User
    {
        return $this->security->getUser();
    }

    public function getUser(int $id): User
    {
        $user = $this->users->find($id);

        if (!$user) {
            throw new NotFoundHttpException("User not found");
        }

        return $user;
    }

    public function delete(int $id): void
    {
        $user = $this->getUser($id);
        $user->setStatus(User::USER_DELETED);

        $this->em->flush();
    }

    public function edit(EditUserRequest $request): User
    {
        $user = $this->users->find($request->getId());

        if (!$user) {
            throw new NotFoundHttpException("User not found");
        }

        if ($user->getFullname() !== $request->getFullName()) {
            $user->setFullname($request->getFullName());
        }

        if ($user->getRole()->getId() !== $request->getRoleId()) {
            $role = $this->em->getRepository(Role::class)->find($request->getRoleId());

            if (!$role) {
                throw new NotFoundHttpException("Role not found");
            }

            $user->setRole($this->em->getRepository(Role::class)->find($request->getRoleId()));
        }

        if ($user->getTokenLifeTime() !== $request->getTokenLifeTime()) {
            $user->setTokenLifeTime($request->getTokenLifeTime());
        }


        $user->removeDifferentUserContact(array_map(function (Contact $contact) {
            return $contact->getId();
        }, $request->getContacts()));

        $contacts = $user->getUserContacts()->toArray();
        foreach ($request->getContacts() as $contact) {
            if (!$contact->hasId()) {
                $userContact = new UserContact($contact->getContactType(), $contact->getContact(), $this->tokenizer->generate());
                $user->addUserContact($userContact);
                $this->em->persist($userContact);
            } else {
                for ($i = 0; $i < count($contacts); $i++) {
                    $uc = $contacts[$i];
                    if ($uc->getId() === $contact->getId()) {
                        $uc->setType($contact->getContactType());
                        $uc->setContact($contact->getContact());
                    }
                }

            }
        }

        $this->em->flush();

        return $user;
    }
}