<?php


namespace App\Service\SearchService;


use App\Dto\GetUserRequest;
use App\Dto\RequestDto;
use App\Service\OptionsService\UserOptionsService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SearchService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var string
     */
    private $optionServiceName;

    public function __construct(EntityManagerInterface $entityManager, string $optionServiceName)
    {
        $this->entityManager = $entityManager;
        $this->optionServiceName = $optionServiceName;
    }

    public function search(RequestDto $options): ArrayCollection
    {
        $builder = $this->entityManager->createQueryBuilder();
        $result = (new $this->optionServiceName($options, $builder))->execute();

        if (!$result) {
            throw new NotFoundHttpException('Not found');
        }

        return $result;
    }
}