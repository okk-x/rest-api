<?php


namespace App\Service\SearchService;


use App\Dto\RequestDto;
use App\Service\OptionsService\UserOptionsService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class SearchUserService extends SearchService
{
    /**
     * @var int|null
     */
    protected $total;

    /**
     * @var int|null
     */
    protected $start;

    /**
     * @var int|null
     */
    protected $limit;

    public function __construct(EntityManagerInterface $entityManager, Security $security, string $optionServiceName = null)
    {
        $optionServiceName = UserOptionsService::class;
        parent::__construct($entityManager, $optionServiceName);
    }

    public function search(RequestDto $options): ArrayCollection
    {
        $users = parent::search($options);
        $pagination = $options->getPagination();
        $this->start = $pagination->getStart();
        $this->limit = $pagination->getLimit();
        $this->total = $users->count();

        return $users;
    }

    /**
     * @return int|null
     */
    public function getStart(): ?int
    {
        return $this->start;
    }

    /**
     * @return int|null
     */
    public function getTotal(): ?int
    {
        return $this->total;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }
}