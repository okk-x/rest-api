<?php


namespace App\Service\SearchService;


use App\Service\OptionsService\RoleOptionsService;
use Doctrine\ORM\EntityManagerInterface;

class SearchRoleService extends SearchService
{
    public function __construct(EntityManagerInterface $entityManager, string $optionServiceName = null)
    {
        $optionServiceName = RoleOptionsService::class;
        parent::__construct($entityManager, $optionServiceName);
    }
}