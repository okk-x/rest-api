<?php


namespace App\Service\SearchService;


use App\Service\OptionsService\RightOptionsService;
use Doctrine\ORM\EntityManagerInterface;

class SearchRightService extends SearchService
{
    public function __construct(EntityManagerInterface $entityManager, string $optionServiceName = null)
    {
        $optionServiceName = RightOptionsService::class;
        parent::__construct($entityManager, $optionServiceName);
    }
}