<?php


namespace App\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class ApiResponse extends JsonResponse
{
    public const CLIENT_ERROR_STATUS = 400;

    public static function success(array $data = []): self
    {
        return new static(['data' => $data]);
    }

    public static function error(string $code, string $message): self
    {
        return new static([
            'code' => $code,
            'message' => $message
        ], self::CLIENT_ERROR_STATUS);
    }

    public static function unauthorized(string $code, string $message): self
    {
        return new static([
            'code' => $code,
            'message' => $message
        ], self::HTTP_UNAUTHORIZED);
    }


}