<?php

namespace App\Repository;

use App\Entity\RoleRight;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RoleRight|null find($id, $lockMode = null, $lockVersion = null)
 * @method RoleRight|null findOneBy(array $criteria, array $orderBy = null)
 * @method RoleRight[]    findAll()
 * @method RoleRight[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoleRightRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RoleRight::class);
    }

    // /**
    //  * @return RoleRight[] Returns an array of RoleRight objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RoleRight
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
