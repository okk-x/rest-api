<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\UserContact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findAuthUser(int $type, string $contact, string $hashPassword): ?User
    {
        return $this->createQueryBuilder('u')
            ->innerJoin('u.userContacts', 'uc')
            ->andWhere('uc.type=:type and uc.contact=:contact
                          and u.password=:password
                          and uc.is_verified=' . UserContact::VERIFIED . ' and u.status=' . User::USER_ACTIVE)
            ->setParameter(":type", $type)
            ->setParameter(':contact', $contact)
            ->setParameter(':password', $hashPassword)
            ->getQuery()->getOneOrNullResult();
    }

    public function findByDeviceUuid(string $deviceUuid): ?User
    {
        return $this->createQueryBuilder('u')
            ->innerJoin('u.userTokens', 'ut')
            ->andWhere('ut.device_uuid=:device_uuid')
            ->setParameter(':device_uuid', $deviceUuid)
            ->getQuery()->getOneOrNullResult();
    }

    public function hasRight(User $user, string $right): bool
    {
        return $this->createQueryBuilder('u')
            ->select('COUNT(u)')
            ->innerJoin('u.role', 'role')
            ->innerJoin('role.role_right', 'rl')
            ->innerJoin('rl.right', 'r')
            ->andWhere('r.alias=:right')
            ->andWhere('u.id=:id')
            ->setParameter(':right', $right)
            ->setParameter(':id', $user->getId())
            ->getQuery()->getSingleScalarResult();
    }

    public function findByIdAndToken(int $userId, string $token): ?User
    {
        return $this->createQueryBuilder('u')
            ->select('u,r')
            ->innerJoin('u.role', 'r')
            ->innerJoin('u.userTokens', 'ut')
            ->andWhere('u.id=:id')
            ->andWhere('ut.token=:token')
            ->setParameter(':id', $userId)
            ->setParameter(':token', $token)
            ->getQuery()->getOneOrNullResult();
    }

    /* public function findByContactDetails(int $contactType,string $contact)
     {
         $this->createQueryBuilder('u')
             ->and
     }*/

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
