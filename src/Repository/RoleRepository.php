<?php

namespace App\Repository;

use App\Entity\Role;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Role|null find($id, $lockMode = null, $lockVersion = null)
 * @method Role|null findOneBy(array $criteria, array $orderBy = null)
 * @method Role[]    findAll()
 * @method Role[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoleRepository extends ServiceEntityRepository
{
    /**
     * @var int
     */
    private $defaultRoleId;
    /**
     * @var int
     */
    private $defaultRoleAlias;

    public function __construct(ManagerRegistry $registry, string $defaultRoleAlias)
    {
        parent::__construct($registry, Role::class);
        $this->defaultRoleAlias = $defaultRoleAlias;
    }

    public function defaultRole(): Role
    {
        return $this->findOneBy(['alias'=>$this->defaultRoleAlias]);
    }


    // /**
    //  * @return RoleWithRights[] Returns an array of RoleWithRights objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RoleWithRights
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
