<?php


namespace App\Controller\Api;


use App\Controller\ApiController;
use App\Dto\GetRightRequest;
use App\Dto\RightsResponseDto;
use App\Response\ListRightsResponse;
use App\Security\UserVoter;
use App\Service\SearchService\SearchRightService;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

class RightController extends ApiController
{
    /**
     * @OA\Get(path="/rights",
     *          tags={"Right"},
     *          @OA\Parameter(
     *              in="query",
     *              name="body",
     *              @OA\Schema(ref="#/components/schemas/GetRightRequest")
     *          ),
     *          @OA\Response(
     *              response="200",
     *              description="Returned list of rights",
     *              @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(ref="#/components/schemas/RightsResponseDto")
     *              )
     *          )
     *)
     * @Route("/api/rights",methods={"GET"})
     * @param GetRightRequest $request
     * @param SearchRightService $service
     * @return RightsResponseDto
     */
    public function search(GetRightRequest $request, SearchRightService $service)
    {
        $this->denyAccessUnlessGranted(UserVoter::RIGHT_READ);
        $rights = $service->search($request);

        return new RightsResponseDto($rights, $request->getRoleId());
    }

}