<?php


namespace App\Controller\Api;


use App\Controller\ApiController;
use App\Dto\AddUserRequest;
use App\Dto\DeleteUserRequest;
use App\Dto\EditUserRequest;
use App\Dto\FullUserResponseDto;
use App\Dto\FullUsersResponseDto;
use App\Dto\GetUserRequest;
use App\Dto\PaginationDto;
use App\Dto\ShowUserRequest;
use App\Dto\SuccessResponseDto;
use App\Dto\UserResponseDto;
use App\Response\FullInfoUsersResponse;
use App\Security\UserVoter;
use App\Service\AuthService;
use App\Service\SearchService\SearchUserService;
use App\Service\UserService;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;




class UserController extends ApiController
{
    /**
     * @OA\Post(path="/user",
     *          tags={"User"},
     *          @OA\Parameter(
     *              in="query",
     *              name="body",
     *              required=true,
     *              @OA\Schema(ref="#/components/schemas/AddUserRequest")
     *          ),
     *          @OA\Response(
     *              response="200",
     *              description="Returned ID auth user",
     *              @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(ref="#/components/schemas/UserResponseDto")
     *              )
     *          )
     * )
     * @Route("/api/user",name="add-user",methods={"POST"})
     * @param AddUserRequest $request
     * @param AuthService $service
     */
    public function add(AddUserRequest $request, AuthService $service)
    {
        $this->denyAccessUnlessGranted(UserVoter::USER_CREATE);
        $user = $service->add($request);

        return new UserResponseDto($user->getId());
    }

    /**
     * @OA\Get(path="/user",
     *          tags={"User"},
     *          @OA\Parameter(
     *              in="query",
     *              name="body",
     *              @OA\Schema(ref="#/components/schemas/ShowUserRequest")
     *          ),
     *          @OA\Response(
     *              response="200",
     *              description="Returned full information about user",
     *              @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(ref="#/components/schemas/FullUserResponseDto")
     *              )
     *          )
     * )
     * @Route("/api/user",methods={"GET"})
     * @param ShowUserRequest $request
     * @param UserService $service
     */
    public function show(ShowUserRequest $request, UserService $service)
    {
        if ($request->hasId()) {
            $user = $service->getUser($request->getId());
            $this->denyAccessUnlessGranted(UserVoter::USER_READ, $user);
        } else {
            $user = $service->getAuthUser();
        }

        return new FullUserResponseDto($user);
    }

    /**
     * @OA\Get(path="/users",
     *          tags={"User"},
     *          @OA\Parameter(
     *              in="query",
     *              name="body",
     *              @OA\Schema(ref="#/components/schemas/GetUserRequest")
     *          ),
     *          @OA\Response(
     *              response="200",
     *              description="Returned user list and pagination",
     *              @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(ref="#/components/schemas/UserResponseDto")
     *              )
     *          )
     * )
     * @Route("/api/users",methods={"GET"})
     * @param GetUserRequest $request
     * @param SearchUserService $service
     */
    public function search(GetUserRequest $request, SearchUserService $service)
    {
        $users = $service->search($request);
        return new FullUsersResponseDto($users,
            new PaginationDto($service->getStart(), $service->getLimit(), $service->getTotal()));
    }

    /**
     * @OA\Put(path="/user",
     *          tags={"User"},
     *          @OA\Parameter(
     *              in="query",
     *              name="body",
     *              required=true,
     *              @OA\Schema(ref="#/components/schemas/EditUserRequest")
     *          ),
     *          @OA\Response(
     *              response="200",
     *              description="Returned ID auth user",
     *              @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(ref="#/components/schemas/UserResponseDto")
     *              )
     *          )
     * )
     * @Route("/api/user",methods={"PUT"})
     * @param EditUserRequest $request
     * @param UserService $service
     */
    public function edit(EditUserRequest $request, UserService $service)
    {
        $this->denyAccessUnlessGranted(UserVoter::USER_EDIT);
        $user = $service->edit($request);

        return new UserResponseDto($user->getId());
    }

    /**
     * @OA\Delete(path="/user",
     *          tags={"User"},
     *          @OA\Parameter(
     *              in="query",
     *              name="body",
     *              required=true,
     *              @OA\Schema(ref="#/components/schemas/DeleteUserRequest")
     *          ),
     *          @OA\Response(
     *              response="200",
     *              description="Returned success status on logout",
     *              @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(ref="#/components/schemas/SuccessResponseDto")
     *              )
     *          )
     * )
     * @Route("/api/user",methods={"DELETE"})
     * @param DeleteUserRequest $request
     * @param UserService $service
     */
    public function delete(DeleteUserRequest $request, UserService $service)
    {
        $this->denyAccessUnlessGranted(UserVoter::USER_DELETE);
        $service->delete($request->getId());

        return new SuccessResponseDto(true);
    }





}