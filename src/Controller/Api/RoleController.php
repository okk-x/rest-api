<?php


namespace App\Controller\Api;


use App\Controller\ApiController;
use App\Dto\GetRoleRequest;
use App\Dto\RolesResponseDto;
use App\Entity\Role;
use App\Response\ApiResponse;
use App\Response\ListRolesResponse;
use App\Security\UserVoter;
use App\Service\SearchService\SearchRoleService;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

class RoleController extends ApiController
{
    /**
     *  @OA\Get(path="/roles",
     *          tags={"Role"},
     *          @OA\Parameter(
     *              in="query",
     *              name="body",
     *              @OA\Schema(ref="#/components/schemas/GetRoleRequest")
     *          ),
     *          @OA\Response(
     *              response="200",
     *              description="Returned list of rights",
     *              @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(ref="#/components/schemas/RolesResponseDto")
     *              )
     *          )
     *)
     * @Route("/api/roles", methods={"GET"})
     * @param GetRoleRequest $request
     * @param SearchRoleService $service
     */
    public function search(GetRoleRequest $request, SearchRoleService $service)
    {
        $this->denyAccessUnlessGranted(UserVoter::ROLE_READ);
        $roles = $service->search($request);

        return new RolesResponseDto($roles);
    }
}