<?php

namespace App\Controller\Api;

use App\Controller\ApiController;
use App\Dto\SignUpRequest;
use App\Dto\SuccessResponseDto;
use App\Dto\UserResponseDto;
use App\Dto\VerificationRequest;
use App\Service\AuthService;
use App\Service\VerificationService;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

class SignUpController extends ApiController
{

    /**
     * @OA\Post(path="/singup",
     *          tags={"Sign up"},
     *          @OA\Parameter(
     *              in="query",
     *              name="body",
     *              required=true,
     *              @OA\Schema(ref="#/components/schemas/SignUpRequest")
     *          ),
     *          @OA\Response(
     *              response="200",
     *              description="Returned ID auth user",
     *              @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(ref="#/components/schemas/UserResponseDto")
     *              )
     *          )
     * )
     * @Route("/api/signup", name="sign_up",methods={"POST"})
     * @param SignUpRequest $request
     * @param AuthService $auth
     * @param VerificationService $verification
     */
    public function request(SignUpRequest $request,
                            AuthService $auth,
                            VerificationService $verification)
    {

        $user = $auth->register($request);

        $verification->sendToEmail($user);
        return new UserResponseDto($user->getId());
    }

    /**
     * @OA\Post(path="/verification",
     *          tags={"Sign up"},
     *          @OA\Parameter(
     *              in="query",
     *              name="body",
     *              required=true,
     *              @OA\Schema(ref="#/components/schemas/VerificationRequest")
     *          ),
     *          @OA\Response(
     *              response="200",
     *              description="Returned success status on verification",
     *              @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(ref="#/components/schemas/SuccessResponseDto")
     *              )
     *          )
     * )
     * @Route("/api/verification",name="verification", methods={"POST"})
     * @param VerificationRequest $request
     * @param VerificationService $verificationService
     */
    public function verification(VerificationRequest $request, VerificationService $verificationService)
    {
        $verificationService->process($request->getId(), $request->getVerificationToken());
        return new SuccessResponseDto(true);
    }


}
