<?php


namespace App\Controller\Api;


use App\Controller\ApiController;
use App\Dto\AuthorizationRequest;
use App\Dto\AuthResponseDto;
use App\Dto\LogoutRequest;
use App\Dto\SuccessResponseDto;
use App\Service\AuthService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;


class AuthorizationController extends ApiController
{
    /**
     * @OA\Post(path="/login",
     *          tags={"Auth"},
     *          @OA\Parameter(
     *              in="query",
     *              name="body",
     *              required=true,
     *              @OA\Schema(ref="#/components/schemas/AuthorizationRequest")
     *          ),
     *          @OA\Response(
     *              response="200",
     *              description="Returned information about auth user",
     *              @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(ref="#/components/schemas/AuthResponseDto")
     *              )
     *          )
     *)
     * @Route("/api/login",name="login",methods={"POST"})
     * @param AuthorizationRequest $authRequest
     * @param Request $request
     * @param AuthService $service
     */
    public function login(AuthorizationRequest $authRequest, Request $request, AuthService $service)
    {
        $token = $service->authorization($authRequest, $request->getClientIp());
        return new AuthResponseDto($token);
    }

    /**
     * @OA\Post(path="/logout",
     *          tags={"Auth"},
     *          @OA\Parameter(
     *              in="query",
     *              name="body",
     *              required=true,
     *              @OA\Schema(ref="#/components/schemas/LogoutRequest")
     *          ),
     *          @OA\Response(
     *              response="200",
     *              description="Returned success status on logout",
     *              @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(ref="#/components/schemas/SuccessResponseDto")
     *              )
     *          )
     *)
     * @Route("/api/logout",name="logout",methods={"POST"})
     * @param LogoutRequest $request
     * @param AuthService $service
     */
    public function logout(LogoutRequest $request, AuthService $service)
    {
        $service->logout($request->getDeviceUuid(), $request->getType());
        return new SuccessResponseDto(true);
    }

}