<?php


namespace App\Controller\Api;

use App\Controller\ApiController;
use App\Dto\RoleRightRelationRequest;
use App\Dto\SuccessResponseDto;
use App\Response\ApiResponse;
use App\Security\UserVoter;
use App\Service\RoleRightService;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

class RoleRightController extends ApiController
{
    /**
     * @OA\Post(path="/role-right",
     *          tags={"RoleRight"},
     *          @OA\Parameter(
     *              in="query",
     *              name="body",
     *              required=true,
     *              @OA\Schema(ref="#/components/schemas/RoleRightRelationRequest")
     *          ),
     *          @OA\Response(
     *              response="200",
     *              description="Returned success status on add new relation",
     *              @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(ref="#/components/schemas/SuccessResponseDto")
     *              )
     *          )
     *)
     * @Route("/api/role-right",methods={"POST"})
     * @param RoleRightRelationRequest $request
     * @param RoleRightService $service
     */
    public function save(RoleRightRelationRequest $request, RoleRightService $service)
    {
        $this->denyAccessUnlessGranted(UserVoter::ROLE_RIGHT_RELATIONS_SAVE);
        $service->save($request);
        return new SuccessResponseDto(true);
    }
}