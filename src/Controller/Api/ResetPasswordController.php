<?php


namespace App\Controller\Api;


use App\Controller\ApiController;
use App\Dto\PasswordRecoveryRequest;
use App\Dto\ResetPasswordRequest;
use App\Dto\SuccessResponseDto;
use App\Response\ApiResponse;
use App\Service\ResettingPasswordService;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

class ResetPasswordController extends ApiController
{
    /**
     * @OA\Post(path="/reset-password",
     *          tags={"Reset password"},
     *          @OA\Parameter(
     *              in="query",
     *              name="body",
     *              required=true,
     *              @OA\Schema(ref="#/components/schemas/PasswordRecoveryRequest")
     *          ),
     *          @OA\Response(
     *              response="200",
     *              description="Returned success status on password recovery",
     *              @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(ref="#/components/schemas/SuccessResponseDto")
     *              )
     *          )
     *)
     * @Route("/api/reset-password",methods={"POST"})
     * @param PasswordRecoveryRequest $request
     * @param ResettingPasswordService $service
     */
    public function reset(PasswordRecoveryRequest $request, ResettingPasswordService $service)
    {
        $service->reset($request);
        return new SuccessResponseDto(true);
    }

    /**
     *@OA\Post(path="/forgot-password",
     *          tags={"Reset password"},
     *          @OA\Parameter(
     *              in="query",
     *              name="body",
     *              required=true,
     *              @OA\Schema(ref="#/components/schemas/ResetPasswordRequest")
     *          ),
     *          @OA\Response(
     *              response="200",
     *              description="Returned success status on reset password",
     *              @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(ref="#/components/schemas/SuccessResponseDto")
     *              )
     *          )
     *)
     * @Route("/api/forgot-password",methods={"POST"})
     * @param ResetPasswordRequest $request
     * @param ResettingPasswordService $service
     */
    public function forgot(ResetPasswordRequest $request, ResettingPasswordService $service)
    {
        $service->requestByEmail($request->getContactType(), $request->getContact());

        return new SuccessResponseDto(true);
    }
}