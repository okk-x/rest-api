<?php


namespace App\Controller;


use App\Response\ApiResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class ApiController extends AbstractController
{
    public function createSuccessResponse(array $data = []): ApiResponse
    {
        return ApiResponse::success($data);
    }

    public function createErrorResponse(string $code, string $message): ApiResponse
    {
        return ApiResponse::error($code, $message);
    }
}