<?php


namespace App\ArgumentResolver;

use App\Dto\RequestDtoInterface;
use App\Exception\ValidationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestDTOResolver implements ArgumentValueResolverInterface
{

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function supports(Request $request, ArgumentMetadata $argument)
    {
        try{
            return (new \ReflectionClass($argument->getType()))
                ->implementsInterface(RequestDtoInterface::class);
        }
        catch(\ReflectionException $exception)
        {
            return false;
        }
    }

    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $class = $argument->getType();
        $dto = new $class($request);

        $errors = $this->validator->validate($dto);

        if (count($errors) > 0)
            throw new ValidationException($errors);

        yield $dto;
    }
}