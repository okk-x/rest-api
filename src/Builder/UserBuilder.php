<?php


namespace App\Builder;


use App\Entity\Role;
use App\Entity\User;
use App\Entity\UserContact;

class UserBuilder
{
    protected $user;

    public function __construct()
    {
        $this->user = new User();
    }

    public function setFullname(string $fullname): self
    {
        $this->user->setFullName($fullname);

        return $this;
    }

    public function setPassword(string $password): self
    {
        $this->user->setPassword($password);

        return $this;
    }

    public function setContact(UserContact $contact): self
    {
        $this->user->addUserContact($contact);

        return $this;
    }

    public function setRole(Role $role): self
    {
        $this->user->setRole($role);

        return $this;
    }

    public function setStatus(int $status)
    {
        if (!in_array($status, $this->user->getUserStatuses())) {
            throw new \Exception('Status not defined');
        }

        $this->user->setStatus($status);

        return $this;
    }

    public function setTokenLifeTime(int $tlt): self
    {
        if ($tlt < 0) {
            throw new \Exception('Token life time must be positive');
        }

        $this->user->setTokenLifeTime($tlt);

        return $this;
    }

    public function build(): User
    {
        if (!$this->user->getFullName()) {
            throw new \Exception('Fullname is required');
        }

        if (!$this->user->getPassword()) {
            throw new \Exception('Password is required');
        }

        if ($this->user->getUserContacts()->isEmpty()) {
            throw new \Exception('Contact is required');
        }

        if (!$this->user->getRole()) {
            throw new \Exception('RoleWithRights is required');
        }
        $tokenLifeTime = $this->user->getTokenLifeTime();
        if (!isset($tokenLifeTime)) {
            throw new \Exception('Token life time is required');
        }

        $status = $this->user->getStatus();
        if (!isset($status)) {
            throw new \Exception('Status is required');
        }

        return $this->user;
    }

    public function reset(): void
    {
        $this->user = new User();
    }
}