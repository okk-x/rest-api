<?php

namespace App\Entity;

use App\Repository\RoleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RoleRepository::class)
 * @ORM\Table(name="roles")
 */
class Role
{
    public const ROLE_USER = 'user';
    public const ROLE_ADMIN = 'admin';
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255,unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255,unique=true)
     */
    private $alias;

    /**
     * @ORM\OneToMany(targetEntity=RoleRight::class, mappedBy="role")
     */
    private $role_right;

    public function __construct(string $name, string $alias)
    {
        $this->name = $name;
        $this->alias = $alias;
        $this->role_right = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function setAlias(string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * @return Collection|RoleRight[]
     */
    public function getRoleRight(): Collection
    {
        return $this->role_right;
    }

    public function addRoleRight(RoleRight $roleRight): self
    {
        if (!$this->role_right->contains($roleRight)) {
            $this->role_right[] = $roleRight;
            $roleRight->setRole($this);
        }

        return $this;
    }

    public function removeRoleRight(RoleRight $roleRight): self
    {
        if ($this->role_right->contains($roleRight)) {
            $this->role_right->removeElement($roleRight);
            // set the owning side to null (unless already changed)
            if ($roleRight->getRole() === $this) {
                $roleRight->setRole(null);
            }
        }

        return $this;
    }
}
