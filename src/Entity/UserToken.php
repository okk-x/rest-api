<?php

namespace App\Entity;

use App\Repository\UserTokenRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserTokenRepository::class)
 * @ORM\Table(name="user_token")
 */
class UserToken
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userTokens")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ip;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $device_uuid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $device_name;

    /**
     * @ORM\Column(type="datetime",nullable=false)
     * @ORM\Version
     */
    private $created_ts;

    /**
     * @ORM\Column(type="datetime",nullable=false)
     * @ORM\Version
     */
    private $update_ts;

    public function __construct(string $token, string $ip, string $device_uuid, string $device_name)
    {
        $this->token = $token;
        $this->ip = $ip;
        $this->device_uuid = $device_uuid;
        $this->device_name = $device_name;
        $this->created_ts = $this->update_ts = new \DateTimeImmutable('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getDeviceUuid(): ?string
    {
        return $this->device_uuid;
    }

    public function setDeviceUuid(string $device_uuid): self
    {
        $this->device_uuid = $device_uuid;

        return $this;
    }

    public function getDeviceName(): ?string
    {
        return $this->device_name;
    }

    public function setDeviceName(string $device_name): self
    {
        $this->device_name = $device_name;

        return $this;
    }

    public function getCreatedTs(): ?\DateTimeInterface
    {
        return $this->created_ts;
    }

    public function setCreatedTs(\DateTimeInterface $created_ts): self
    {
        $this->created_ts = $created_ts;

        return $this;
    }

    public function getUpdateTs(): ?\DateTimeInterface
    {
        return $this->update_ts;
    }

    public function setUpdateTs(\DateTimeInterface $update_ts): self
    {
        $this->update_ts = $update_ts;

        return $this;
    }
}
