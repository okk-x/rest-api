<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
class ResetPassword
{
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="datetime_immutable",nullable=true,name="code_expire")
     */
    private $expires;

    /**
     * ResetPassword constructor.
     * @param string $code
     * @param \DateTimeImmutable $expires
     */
    public function __construct(string $code, \DateTimeImmutable $expires)
    {
        $this->code = $code;
        $this->expires = $expires;
    }

    public function isExpiredTo(\DateTimeImmutable $date): bool
    {
        return $this->expires <= $date;
    }

    public function isEmpty()
    {
        return empty($this->code);
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getExpires(): \DateTimeImmutable
    {
        return $this->expires;
    }

    public function clear(): void
    {
        $this->code = null;
        $this->expires = null;
    }
}