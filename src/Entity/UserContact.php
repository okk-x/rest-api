<?php

namespace App\Entity;

use App\Repository\UserContactRepository;
use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

/**
 * @ORM\Entity(repositoryClass=UserContactRepository::class)
 * @ORM\Table(name="user_contacts")
 */
class UserContact
{
    public const VERIFIED = 0;
    public const NOT_VERIFIED = 1;

    public const TYPE_EMAIL = 1;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userContacts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255,unique=true)
     */
    private $contact;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_verified;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $verification_token;

    public function __construct(int $type, string $contact, string $verificationToken)
    {
        Assert::oneOf($type, [
            self::TYPE_EMAIL
        ]);

        $this->type = $type;
        $this->contact = $contact;
        $this->is_verified = self::NOT_VERIFIED;
        $this->verification_token = $verificationToken;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function setContact(string $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getIsVerified(): ?bool
    {
        return $this->is_verified;
    }

    public function isVerified(): bool
    {
        return (int)$this->is_verified === self::VERIFIED;
    }

    public function setIsVerified(bool $is_verified): self
    {
        $this->is_verified = $is_verified;

        return $this;
    }

    public function getVerificationToken(): ?string
    {
        return $this->verification_token;
    }

    public function setVerificationToken(string $verification_token): self
    {
        $this->verification_token = $verification_token;

        return $this;
    }
}
