<?php

namespace App\Entity;

use App\Repository\ApiKeyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ApiKeyRepository::class)
 * @ORM\Table(name="api_keys")
 */
class ApiKey
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="apiKey", cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $key_uuid;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $key_secret;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    /**
     * @ORM\Column(type="datetime",nullable=false)
     * @ORM\Version
     */
    private $created_ts;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getKeyUuid(): ?string
    {
        return $this->key_uuid;
    }

    public function setKeyUuid(string $key_uuid): self
    {
        $this->key_uuid = $key_uuid;

        return $this;
    }

    public function getKeySecret(): ?string
    {
        return $this->key_secret;
    }

    public function setKeySecret(string $key_secret): self
    {
        $this->key_secret = $key_secret;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedTs(): ?\DateTimeInterface
    {
        return $this->created_ts;
    }

    public function setCreatedTs(\DateTimeInterface $created_ts): self
    {
        $this->created_ts = $created_ts;

        return $this;
    }
}
