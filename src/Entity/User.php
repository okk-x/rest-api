<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="users")
 */
class User implements UserInterface
{
    public const USER_ACTIVE = 0;
    public const USER_NOT_ACTIVE = 1;
    public const USER_DELETED = 2;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $full_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;


    /**
     * @ORM\ManyToOne(targetEntity=Role::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $role;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @ORM\Column(type="integer")
     */
    private $token_life_time;

    /**
     * @ORM\Embedded(class="ResetPassword",columnPrefix="password_restore_")
     */
    private $resetPassword;

    /**
     * @ORM\Column(type="datetime",nullable=false)
     * @ORM\Version
     */
    private $created_ts;

    /**
     * @ORM\Column(type="datetime",nullable=false)
     * @ORM\Version
     */
    private $last_login_ts;

    /**
     * @ORM\OneToMany(targetEntity=UserContact::class, mappedBy="user", orphanRemoval=true, cascade={"persist"})
     */
    private $userContacts;

    /**
     * @ORM\OneToMany(targetEntity=UserToken::class, mappedBy="user", orphanRemoval=true,cascade={"persist"})
     */
    private $userTokens;

    /**
     * @ORM\OneToOne(targetEntity=ApiKey::class, mappedBy="user", cascade={"persist", "remove"})
     */
    private $apiKey;

    public function __construct()
    {
        $this->created_ts = $this->last_login_ts = new \DateTime('now');
        $this->userContacts = new ArrayCollection();
        $this->userTokens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->full_name;
    }

    public function setFullName(string $full_name): self
    {
        $this->full_name = $full_name;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRole(): ?Role
    {
        return $this->role;
    }

    public function setRole(?Role $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public static function getUserStatuses()
    {
        return [USER::USER_ACTIVE, USER::USER_NOT_ACTIVE, USER::USER_DELETED];
    }

    public function getTokenLifeTime(): ?int
    {
        return $this->token_life_time;
    }

    public function setTokenLifeTime(int $token_life_time): self
    {
        $this->token_life_time = $token_life_time;

        return $this;
    }

    public function setResetPassword(ResetPassword $resetPassword): self
    {
        $this->resetPassword = $resetPassword;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResetPassword(): ?ResetPassword
    {
        return $this->resetPassword;
    }

    /**
     * @ORM\PostLoad()
     */
    public function checkEmbeds()
    {
        if ($this->resetPassword->isEmpty())
            $this->resetPassword = null;
    }

    public function getCreatedTs(): ?\DateTimeInterface
    {
        return $this->created_ts;
    }

    public function setCreatedTs(\DateTimeInterface $created_ts): self
    {
        $this->created_ts = $created_ts;

        return $this;
    }

    public function getLastLoginTs(): ?\DateTimeInterface
    {
        return $this->last_login_ts;
    }

    public function setLastLoginTs(\DateTimeInterface $last_login_ts): self
    {
        $this->last_login_ts = $last_login_ts;

        return $this;
    }

    /**
     * @return Collection|UserContact[]
     */
    public function getUserContacts(): Collection
    {
        return $this->userContacts;
    }

    public function addUserContact(UserContact $userContact): self
    {
        if (!$this->userContacts->contains($userContact)) {
            $this->userContacts[] = $userContact;
            $userContact->setUser($this);
        }

        return $this;
    }

    public function removeUserContact(UserContact $userContact): self
    {
        if ($this->userContacts->contains($userContact)) {
            $this->userContacts->removeElement($userContact);
            // set the owning side to null (unless already changed)
            if ($userContact->getUser() === $this) {
                $userContact->setUser(null);
            }
        }

        return $this;
    }

    public function removeDifferentUserContact(array $ids): void
    {
        $ucIds = array_map(function (UserContact $contact) {
            return $contact->getId();
        }, $this->userContacts->toArray());

        $diff = array_diff($ids, $ucIds);

        foreach ($this->userContacts as $contact) {
            if (in_array($contact->getId(), $diff)) {
                $this->removeUserContact($contact);
            }
        }
    }

    /**
     * @return Collection|UserToken[]
     */
    public function getUserTokens(): Collection
    {
        return $this->userTokens;
    }

    public function addUserToken(UserToken $userToken): self
    {
        if (!$this->userTokens->contains($userToken)) {
            $this->userTokens[] = $userToken;
            $userToken->setUser($this);
        }

        return $this;
    }

    public function removeUserToken(UserToken $userToken): self
    {
        if ($this->userTokens->contains($userToken)) {
            $this->userTokens->removeElement($userToken);
            // set the owning side to null (unless already changed)
            if ($userToken->getUser() === $this) {
                $userToken->setUser(null);
            }
        }

        return $this;
    }

    public function getApiKey(): ?ApiKey
    {
        return $this->apiKey;
    }

    public function setApiKey(?ApiKey $apiKey): self
    {
        $this->apiKey = $apiKey;

        // set (or unset) the owning side of the relation if necessary
        $newUser = null === $apiKey ? null : $this;
        if ($apiKey->getUser() !== $newUser) {
            $apiKey->setUser($newUser);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getPlainPassword(): string
    {
        return $this->plainPassword;
    }

    public function getRoles()
    {
        //return ['ROLE_'.strtoupper($this->role->getAlias())];
        return ['ROLE_USER'];
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return $this->full_name;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}
