<?php

namespace App\Entity;

use App\Repository\RoleRightRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RoleRightRepository::class)
 * @ORM\Table(name="role_rights",uniqueConstraints={
 *      @ORM\UniqueConstraint(columns={"right_id","role_id"})
 * })
 */
class RoleRight
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity=Right::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $right;

    /**
     * @ORM\ManyToOne(targetEntity=Role::class, inversedBy="role_right")
     * @ORM\JoinColumn(nullable=false)
     */
    private $role;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRole(): ?Role
    {
        return $this->role;
    }

    public function setRole(?Role $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getRight(): ?Right
    {
        return $this->right;
    }

    public function setRight(?Right $right): self
    {
        $this->right = $right;

        return $this;
    }
}
