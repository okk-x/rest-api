<?php


namespace App\Security;


use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserVoter extends Voter
{
    public const USER_CREATE = 'users.create';
    public const USER_READ = 'users.read';
    public const USER_READ_DELETED = 'users.read-deleted';
    public const USER_EDIT = 'users.edit';
    public const USER_DELETE = 'users.delete';
    public const ROLE_READ = 'roles.read';
    public const RIGHT_READ = 'rights.read';
    public const ROLE_RIGHT_RELATIONS_SAVE = 'roles.rights-relations-save';

    /**
     * @var UserRepository
     */
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    protected function supports(string $attribute, $subject = null)
    {
        if (!$this->attributeExists($attribute)) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        if (isset($subject) && ($subject instanceof User) && $subject->getId() !== $user->getId()) {
            return false;
        }

        return $this->repository->hasRight($user, $attribute);
    }

    protected function attributeExists(string $attribute): bool
    {
        $constants = (new \ReflectionClass($this))->getConstants();
        return in_array($attribute, array_values($constants));
    }

}