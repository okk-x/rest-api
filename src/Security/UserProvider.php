<?php


namespace App\Security;


use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface
{
    /**
     * @var UserRepository
     */
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function loadUserByUsername(string $username)
    {
        return $this->repository->findOneBy(['full_name' => $username]);
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException('Invalid user class in method ' . __METHOD__ . ' in class ' . __CLASS__);
        }
        return $this->repository->findOneBy(['full_name' => $user->getUsername()]);
    }

    public function supportsClass(string $class)
    {
        return $class === User::class;
    }
}