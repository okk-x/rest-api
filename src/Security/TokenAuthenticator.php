<?php


namespace App\Security;


use App\Entity\User;
use App\Response\ApiResponse;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class TokenAuthenticator extends AbstractGuardAuthenticator
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @inheritDoc
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return ApiResponse::unauthorized('401', 'Authentication required');
    }

    /**
     * @inheritDoc
     */
    public function supports(Request $request)
    {
        $route = $request->get('_route');

        if ($route === 'sign_up' || $route === 'login' || $route === "add-user" || $route === "verification") {
            return false;
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getCredentials(Request $request)
    {
        if (!$header = $request->headers->get('X-USER-AUTH')) {
            throw new AuthenticationException('Auth header not defined', 401);
        }

        if (strpos($header, ';') === false) {
            throw new AuthenticationException('Incorrect header format', 401);
        }

        list($userId, $token) = explode(';', $header);
        return [
            'user_id' => $userId,
            'token' => $token
        ];
    }

    /**
     * @inheritDoc
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $user = $this->em->getRepository(User::class)
            ->findByIdAndToken($credentials['user_id'], $credentials['token']);

        if (!$user) {
            throw new AuthenticationException('Auth user not found');
        }

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return ApiResponse::unauthorized($exception->getCode(), $exception->getMessage());
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function supportsRememberMe()
    {
        return false;
    }
}