<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DependsOnType extends Constraint
{
    public $message = "{{ contact }} is incorrect.";

    public function validatedBy()
    {
        return get_class($this) . 'Validator';
    }
}