<?php


namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ConfirmPasswordValidator extends ConstraintValidator
{

    public function validate($value, Constraint $constraint)
    {
        $password = $this->context->getRoot()->getPassword();
        if ($value !== $password) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}