<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CheckFullNameValidator extends ConstraintValidator
{

    public function validate($value, Constraint $constraint)
    {
        if (!preg_match('/^([A-ZА-ЯЁ][a-zа-яё]+[\s]([A-ZА-ЯЁa-za-яё]+[\'-]?)?[A-ZА-Я][a-zа-я]+)$/u', $value)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}