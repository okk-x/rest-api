<?php


namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


/**
 * @Annotation
 */
class CheckFullName extends Constraint
{
    public $message = "Full name consists of letters only and can contain a space.";

    public function validatedBy()
    {
        return get_class($this) . 'Validator';
    }
}