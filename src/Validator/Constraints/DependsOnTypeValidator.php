<?php


namespace App\Validator\Constraints;


use App\Entity\UserContact;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DependsOnTypeValidator extends ConstraintValidator
{

    public function validate($value, Constraint $constraint)
    {
        $object = $this->context->getRoot();
        if (method_exists($object, 'getContactType')) {
            $type = $object->getContactType();
        } else if (method_exists($object, 'getContacts')) {
            $contacts = $object->getContacts();
            foreach ($contacts as $contact) {
                if ($contact->getContact() === $value) {
                    $type = $contact->getContactType();
                    break;
                }
            }
        }

        switch ($type) {
            case UserContact::TYPE_EMAIL:
                if (!filter_var($value, FILTER_VALIDATE_EMAIL))
                    $this->context->buildViolation($constraint->message)
                        ->setParameter('{{ contact }}', $value)
                        ->addViolation();
                break;
            default:
                $this->context->buildViolation('Type does not exist.');
        }
    }
}