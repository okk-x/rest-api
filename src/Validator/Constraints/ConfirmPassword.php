<?php


namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ConfirmPassword extends Constraint
{
    public $message = "Password mismatch.";

    public function validatedBy()
    {
        return get_class($this) . 'Validator';
    }
}