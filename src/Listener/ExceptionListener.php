<?php


namespace App\Listener;


use App\Response\ApiResponse;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionListener
{
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        if ($exception instanceof UniqueConstraintViolationException) {
            $response = ApiResponse::error('409', 'Duplication');
        } else {
            $response = ApiResponse::error($exception->getCode(), $exception->getMessage());
        }
        $event->setResponse($response);
    }
}