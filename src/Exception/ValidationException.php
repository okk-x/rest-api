<?php


namespace App\Exception;


use Symfony\Component\Validator\ConstraintViolationListInterface;
use Throwable;

class ValidationException extends \Exception
{
    /**
     * @var ConstraintViolationListInterface
     */
    private $violations;

    /**
     * ValidationException constructor.
     * @param ConstraintViolationListInterface $violations
     */
    public function __construct(ConstraintViolationListInterface $violations)
    {
        $this->violations = $violations;
        parent::__construct($this->getViolationMessage(), 400);
    }

    public function getViolationMessage(): string
    {
        $message = '';
        foreach ($this->violations as $violation) {
            $message .= $violation->getMessage() . ' ';
        }
        return $message;
    }
}