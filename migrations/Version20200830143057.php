<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200830143057 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO `roles` VALUES(NULL,'Пользователи','users')");
        $this->addSql("INSERT INTO `rights` VALUES(NULL,'Создание пользователей','users.create')");
        $this->addSql("INSERT INTO `rights` VALUES(NULL,'Получение информации о пользователях','users.read')");
        $this->addSql("INSERT INTO `rights` VALUES(NULL,'Получение информации о удаленных пользователях','users.read-deleted')");
        $this->addSql("INSERT INTO `rights` VALUES(NULL,'Редактирование пользователя ','users.edit')");
        $this->addSql("INSERT INTO `rights` VALUES(NULL,'Удаление пользователей','users.delete')");
        $this->addSql("INSERT INTO `rights` VALUES(NULL,'Получение информации о ролях','roles.read')");
        $this->addSql("INSERT INTO `rights` VALUES(NULL,'Получение информации о привилегиях','rights.read')");
        $this->addSql("INSERT INTO `rights` VALUES(NULL,'Сохранение связей между ролью и привилегиями','roles.rights-relations-save')");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("DELETE FROM `roles` WHERE alias='users'");
        $this->addSql("DELETE FROM `rights` WHERE alias='users.create'");
        $this->addSql("DELETE FROM `rights` WHERE alias='users.read'");
        $this->addSql("DELETE FROM `rights` WHERE alias='users.read-deleted'");
        $this->addSql("DELETE FROM `rights` WHERE alias='users.edit'");
        $this->addSql("DELETE FROM `rights` WHERE alias='users.delete'");
        $this->addSql("DELETE FROM `rights` WHERE alias='roles.read'");
        $this->addSql("DELETE FROM `rights` WHERE alias='rights.read'");
        $this->addSql("DELETE FROM `rights` WHERE alias='roles.rights-relations-save'");

    }
}
