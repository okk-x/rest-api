<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200830070323 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE api_keys (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, key_uuid VARCHAR(255) NOT NULL, key_secret VARCHAR(100) NOT NULL, status TINYINT(1) NOT NULL, created_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL, UNIQUE INDEX UNIQ_9579321FA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `rights` (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, alias VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_160D1035E237E06 (name), UNIQUE INDEX UNIQ_160D103E16C6B94 (alias), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role_rights (id INT AUTO_INCREMENT NOT NULL, right_id INT NOT NULL, role_id INT NOT NULL, INDEX IDX_AA4630B254976835 (right_id), INDEX IDX_AA4630B2D60322AC (role_id), UNIQUE INDEX UNIQ_AA4630B254976835D60322AC (right_id, role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE roles (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, alias VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_B63E2EC75E237E06 (name), UNIQUE INDEX UNIQ_B63E2EC7E16C6B94 (alias), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_contacts (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, type INT NOT NULL, contact VARCHAR(255) NOT NULL, is_verified TINYINT(1) NOT NULL, verification_token VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_D3CDF1734C62E638 (contact), INDEX IDX_D3CDF173A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_token (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, token VARCHAR(255) NOT NULL, ip VARCHAR(255) NOT NULL, device_uuid VARCHAR(255) NOT NULL, device_name VARCHAR(255) NOT NULL, created_ts DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, update_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_BDF55A63A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, role_id INT NOT NULL, full_name VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, status SMALLINT NOT NULL, token_life_time INT NOT NULL, created_ts DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, last_login_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL, password_restore_code VARCHAR(255) DEFAULT NULL, password_restore_code_expire DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_1483A5E9D60322AC (role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE api_keys ADD CONSTRAINT FK_9579321FA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE role_rights ADD CONSTRAINT FK_AA4630B254976835 FOREIGN KEY (right_id) REFERENCES `rights` (id)');
        $this->addSql('ALTER TABLE role_rights ADD CONSTRAINT FK_AA4630B2D60322AC FOREIGN KEY (role_id) REFERENCES roles (id)');
        $this->addSql('ALTER TABLE user_contacts ADD CONSTRAINT FK_D3CDF173A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE user_token ADD CONSTRAINT FK_BDF55A63A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E9D60322AC FOREIGN KEY (role_id) REFERENCES roles (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE role_rights DROP FOREIGN KEY FK_AA4630B254976835');
        $this->addSql('ALTER TABLE role_rights DROP FOREIGN KEY FK_AA4630B2D60322AC');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E9D60322AC');
        $this->addSql('ALTER TABLE api_keys DROP FOREIGN KEY FK_9579321FA76ED395');
        $this->addSql('ALTER TABLE user_contacts DROP FOREIGN KEY FK_D3CDF173A76ED395');
        $this->addSql('ALTER TABLE user_token DROP FOREIGN KEY FK_BDF55A63A76ED395');
        $this->addSql('DROP TABLE api_keys');
        $this->addSql('DROP TABLE `rights`');
        $this->addSql('DROP TABLE role_rights');
        $this->addSql('DROP TABLE roles');
        $this->addSql('DROP TABLE user_contacts');
        $this->addSql('DROP TABLE user_token');
        $this->addSql('DROP TABLE users');
    }
}
