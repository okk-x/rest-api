<?php

use OpenApi\Annotations as OA;

/**
 * @OA\Info(title="REST API",version="0.1")
 * @OA\Server(
 *     url="http://rest-api/api/"
 * )
 */